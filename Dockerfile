FROM registry.gitlab.com/livia.costa26/question-core-curso-gitlab:dependencies

COPY . .

RUN mkdir -p assets/static \
    && python manage.py collectstatic --noinput

